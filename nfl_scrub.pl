#!/usr/bin/perl
use strict;
use warnings;
#use LWP::UserAgent;
#use HTTP::Request;
#use HTML::TreeBuilder;
use DateTime;
use WWW::Curl::Easy;
use Math::Round;
use Date::Simple ('date', 'today');

use FantasyFlexHelpers;

my $week=get_current_week();
#espn
my $day_of_week = lc(DateTime->now()->day_name);
my $ff_file_base="weeks/" . $week . "/" . $day_of_week . "_";
my $ff_file_ext="nfl.txt";

my $i=1;
my $data_link="http://fantasy.nfl.com/research/projections?";
my $file = $ff_file_base . $ff_file_ext;
my $http;

my $curl = WWW::Curl::Easy->new;
my $response_body;
my $retcode;
my $response_code;
#nfl.com
#positions in same order as cbs positions
my @nfl_positions=("1","2","3","4","7","8");
my $length = scalar(@nfl_positions);
$file = $ff_file_base . $ff_file_ext;
my $offset = 25;
my $max = 800;
#http://fantasy.nfl.com/research/projections?offset=51&position=O&sort=projectedPts&statCategory=projectedStats&statSeason=2014&statType=seasonProjectedStats&statWeek=1
#my $nfl_gets = "statCategory=projectedStats&statSeason=2014&statType=weekProjectedStats&statWeek=";

#http://fantasy.nfl.com/research/projections?
#SEASON
my $nfl_gets = "statCategory=projectedStats&statSeason=2015&statType=weekProjectedStats&statWeek=";
#clean_file($file);
system "echo \"\" > $file";
while($i < $max) {
 $http = $data_link . "offset=" . $i . "&position=0&" . $nfl_gets . $week;
 my $response_body = '';
 $curl->setopt(CURLOPT_HEADER,1);
 $curl->setopt(CURLOPT_URL, $http);

 $curl->setopt(CURLOPT_WRITEDATA,\$response_body);
 $retcode = $curl->perform;
 if ($retcode == 0) {
   print("Transfer went ok\n");
   $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
   # judge result and next action based on $response_code
   open(MYFILE, ">>$file");
   print MYFILE "$response_body";
   print MYFILE "-----\n";
   close(MYFILE);
  } else {
    # Error code, type of error, error message
    print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
  }
  $i = $i + $offset;
  #get_data($http, $file, $curl)
}
