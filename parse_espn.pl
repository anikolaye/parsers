#!/usr/bin/perl

use strict;
use warnings;

use HTML::TreeBuilder::XPath;
use Data::Dumper;
use DBI;
use DateTime;
use Math::Round;

use FantasyFlexHelpers;

my $week=get_current_week();

my $host = "localhost";
my $database = "fantasyflex";
my $port = 5432;
my $user = "postgres";
my $pw = "jBds024jm\@CdxE";

my $source = 2;
my $source_name = 'espn';
my $day_of_week = lc(DateTime->now()->day_name);

print "Today is " . $day_of_week. "\n";

#my $day_of_week='saturday';
#my $day_of_week = "season";

# Establish a database connection
my $dbh = DBI->connect("dbi:Pg:database=$database;host=$host;port=$port;", $user, $pw)
  	or die ( "Couldn't connect to database: " . DBI->errstr );

print "Connected! \n";
# Change the path to the file you want to parse.
#my $tree = HTML::TreeBuilder::XPath->new_from_file('/Users/rhnickels/development/fantasy/parse/downloads/espn.html');
my $start = "../weeks/";
my $middle = "/";
my $end = "_espn.txt";

my $file_to_parse = "weeks/" . $week . "/" . $day_of_week . "_espn.txt";
print $file_to_parse . "\n";
my $tree = HTML::TreeBuilder::XPath->new_from_file($file_to_parse);
# Let's parse the column headers to give definition to the data
my @column_labels;

foreach my $stat_labels ($tree->look_down(_tag => 'tr', 'class' => 'playerTableBgRowSubHead tableSubHead')) {
	foreach my $stat_label ($stat_labels->look_down(_tag => 'td', 'class' => 'playertableStat')) {
		foreach my $label ($stat_label->look_down(_tag => 'span')) {
			print $label->attr('title');
			push @column_labels, $label->attr('title');
		}
	}
	# We only need to run through these once - break out of the loop
	last;
}

# These are the labels in the order we should expect them
my @expected_labels = (
		'Each Pass Completed',
		'Each Pass Attempted',
		'Passing Yards',
		'TD Pass',
		'Interceptions Thrown',
		'Rushing Attempts',
		'Rushing Yards',
		'TD Rush',
		'Each reception',
		'Receiving Yards',
		'TD Reception'
	);

# Lets validate that our labels are accurate and in the correct order before assuming statistical data

# Now that we have an array of labels, lets grab the player data
# Use a pattern rather than a string to match the possible player row classes
my $player_class_pattern = qr/^(?:pncPlayerRow playerTableBgRow0|pncPlayerRow playerTableBgRow1)$/;

my $player_count = 0;

foreach my $player_row ($tree->look_down(_tag => 'tr', 'class' => $player_class_pattern)) {
	# Let's parse his stats
	my $stat_class_pattern = qr/^(?:playertableStat|playertableStat )$/;
	my %stats;
	my $stat_count = 0;

	foreach my $stat ($player_row->look_down(_tag => 'td', 'class' => $stat_class_pattern)) {
		my $column_label = $column_labels[$stat_count];
		# If we're parsing passing completions/attempts (stat_count = 0), lets break that value apart
		if ($stat_count == 0) {
			# Split the string at the forward slash
			my @attempts_completions = split(/\//, $stat->as_text());

			$stats{'Each Pass Completed'} = $attempts_completions[0];
			$stats{'Each Pass Attempted'} = $attempts_completions[1];

			# Increment the counter since we covered two stats here
			$stat_count++;
		} else {
			$stats{$column_label} = $stat->as_text();
		}
		$stat_count++;
	}
    my $full_tag =  $player_row->look_down(_tag => 'td', 'class' => 'playertablePlayerName');
    my $char = ", ";
    my $team = substr($full_tag->as_text(), index($full_tag->as_text(), $char)+2, 2);
    if(lc($team) eq 'gb' or 
    	lc($team) eq 'kc' or
    	lc($team) eq 'ne' or
    	lc($team) eq 'no' or
    	lc($team) eq 'sd' or
    	lc($team) eq 'sf' or
    	lc($team) eq 'tb'
	) {
	} else {
	    $team = substr($full_tag->as_text(), index($full_tag->as_text(), $char)+2, 3);
	}
	if(lc($team) eq 'wsh') {
		$team = 'was';
	}
	if(lc($team) eq 'jax') {
		$team = 'jac';
	}
	# Assign the <a> containing the player's name to a variable
	my $player_name_tag = $player_row->look_down(_tag => 'td', 'class' => 'playertablePlayerName')->look_down(_tag => 'a');
    print "\n player is: " . $player_name_tag->as_text();
    #my $td = qr/^.(?:appliedPoints)./;
	my $projected_score = $player_row->look_down(_tag => 'td', 'class' => 'playertableStat appliedPoints sortedCell');
	#my $projected_score = $player_row->look_down(_tag => 'td', 'class' => qr/^(?:appliedPoints)$/);
	print "\n Projected score:";
	print $projected_score->as_text();
	print "\n";

    my $projected_score_rounded = nearest(1, $projected_score->as_text());
	# Add key->value pairs to the player_hash
	my %player_hash = (
		name => $player_name_tag->as_text(),
         	espn_player_id => $player_name_tag->attr('playerid'),
         	espn_team_id => $player_name_tag->attr('teamid')
      );

	# Dump out the player variables
	print "\n ";
	print Dumper %player_hash;
	print "\n ";
	print Dumper %stats;

	my $player_name = $player_hash{"name"};
	if($player_name eq 'Daniel Herron') {
		$player_name = 'Dan Herron';
	}

	if($player_name eq 'Boobie Dixon') {
		$player_name = 'Anthony Dixon';
	}

	if($player_name eq 'Benjamin Cunningham') {
		$player_name = 'Benny Cunningham';
	}

	if($player_name eq 'Stevie Johnson') {
		$player_name = 'Steve Johnson';
	}
    
    my $player_id = find_player_with_team($player_name, $team, $dbh);

    if ($player_id eq 0) {
        my $print_to_file = "Could not find " . $player_name . " who is projected for:  " . $projected_score_rounded;
        open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
        print MYFILE "$print_to_file\n";
        close (MYFILE);
    } elsif($player_id eq 2) {
        my $print_to_file = "Found more than one instance of " . $player_name . " who is projected for:  " . $projected_score_rounded;
        open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
        print MYFILE "$print_to_file\n";
        close (MYFILE);
    } else {   
        my $insert = save_or_update_projection($player_id, $week, $projected_score, $projected_score_rounded, $day_of_week, $source, $dbh);

        if($insert eq 0) {
            my $print_to_file = $player_name . " is projected for:  " . $projected_score_rounded . " but no gsis_id found";
            open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
            print MYFILE "$print_to_file\n";
            close (MYFILE);
        }
    }

}
print "DONE!";
