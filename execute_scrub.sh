#!/bin/bash
#nfldb-update

cd /home/anikolayevsky/scrapers/ && /usr/local/bin/nfldb-update > nfldboutput.txt && java -jar flex-stat-compiler.jar && perl cbs_scrub.pl && perl espn_scrub.pl && perl fantasypros_scrub.pl && perl fox_scrub.pl && perl nfl_scrub.pl && perl scout_scrub.pl && perl fantasy_sharks_scrub.pl && perl parse_espn.pl && perl parse_fantasypros.pl && perl parse_fox.pl && perl parse_nfl.pl && touch execution_ran
