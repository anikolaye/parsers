#!/usr/bin/perl

use strict;
use warnings;

use HTML::TreeBuilder::XPath;
use Data::Dumper;
use DBI;
use DateTime;
use Math::Round;
use FantasyFlexHelpers;

my $week=get_current_week();

my $host = "localhost";
my $database = "fantasyflex";
my $port = 5432;
my $user = "postgres";
my $pw = "jBds024jm\@CdxE";

my $source = 6;
my $source_name = 'nfl';
my $day_of_week = lc(DateTime->now()->day_name);
#print "Today is " . $day_of_week. "\n";
#my $day_of_week='saturday';
# Establish a database connection
my $dbh = DBI->connect("dbi:Pg:database=$database;host=$host;port=$port;", $user, $pw)
  	or die ( "Couldn't connect to database: " . DBI->errstr );

print "Connected!";
# Change the path to the file you want to parse.
my $file_to_parse = "weeks/" . $week . "/" . $day_of_week . "_nfl.txt";
my $tree = HTML::TreeBuilder::XPath->new_from_file($file_to_parse);

# Lets validate that our labels are accurate and in the correct order before assuming statistical data

# Now that we have an array of labels, lets grab the player data
# Use a pattern rather than a string to match the possible player row classes
my $player_class_pattern = qr/^(?:tableType-player hasGroups)$/;

my $player_count = 0;
#<td>stat projected
#playerCard playerName playerNameFull playerNameId-2501863 what-playerCard">Peyton Manning</a>
#<tbody>

foreach my $tables ($tree->look_down(_tag => 'table', 'class' => $player_class_pattern) ){
    foreach my $player_row ($tables->look_down(_tag=>'tbody')->look_down(_tag => 'tr') ){
        # Assign the <a> containing the player's name to a variable
        my $team = 0;
        my $team_tag = $player_row->look_down(_tag => 'td', 'class' => 'playerNameAndInfo first')->look_down(_tag => 'em');
        my $char = "-";
        $team = substr($team_tag->as_text(), index($team_tag->as_text(), $char)+2);

        my $a_tag_pattern = qr/^(?:playerCard playerName playerNameFull)./;
        my $player_name_tag = $player_row->look_down(_tag => 'td', 'class' => 'playerNameAndInfo first')->look_down(_tag => 'a', 'class'=>$a_tag_pattern);
        print "\n player is: " . $player_name_tag->as_text();

        my $td_tag_pattern = qr/^(?:stat projected)./;
        my $projected_score = $player_row->look_down(_tag => 'td', 'class' => $td_tag_pattern);
        #my $projected_score = $player_row->look_down(_tag => 'td', 'class' => qr/^(?:appliedPoints)$/);
        print "\n Projected score:";
        print $projected_score->as_text();
        print "\n";
        my $projected_score_rounded = nearest(1, $projected_score->as_text());
        # Add key->value pairs to the player_hash
        my %player_hash = (
            name => $player_name_tag->as_text(),
        );

        # Dump out the player variables
        print "\n ";
        print Dumper %player_hash;

        my $player_name = $player_hash{"name"};
        if($player_name eq 'Daniel Herron') {
            $player_name = 'Dan Herron';
        }

        if($player_name eq 'Boobie Dixon') {
            $player_name = 'Anthony Dixon';
        }

        if($player_name eq 'Benjamin Cunningham') {
            $player_name = 'Benny Cunningham';
        }

        if($player_name eq 'Stevie Johnson') {
            $player_name = 'Steve Johnson';
        }

        if($player_name eq 'Tim Wright') {
            $player_name = 'Timothy Wright';
        }

        my $player_id = 0;
        if($team eq '') {
            $player_id = find_player($player_name, $dbh);
        } else {
            $player_id = find_player_with_team($player_name, $team, $dbh);        
        }
        
        if ($player_id eq 0) {
            my $print_to_file = "Could not find " . $player_name . " who is projected for:  " . $projected_score_rounded;
            open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
            print MYFILE "$print_to_file\n";
            close (MYFILE);
        } elsif($player_id eq 2) {
            my $print_to_file = "Found more than one instance of " . $player_name . " who is projected for:  " . $projected_score_rounded;
            open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
            print MYFILE "$print_to_file\n";
            close (MYFILE);
        } else {   
            my $insert = save_or_update_projection($player_id, $week, $projected_score, $projected_score_rounded, $day_of_week, $source, $dbh);

            if($insert eq 0) {
                my $print_to_file = $player_name . " is projected for:  " . $projected_score_rounded . " but no gsis_id found";
                open (MYFILE, '>>weeks/' . $week . '/' .$source_name . '_' . $day_of_week . '_noninserted.txt');
                print MYFILE "$print_to_file\n";
                close (MYFILE);
            }
        }

    }
}
print "DONE!";
