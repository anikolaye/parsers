#!/usr/bin/perl
use strict;
use warnings;
#use LWP::UserAgent;
#use HTTP::Request;
#use HTML::TreeBuilder;
use DateTime;
use WWW::Curl::Easy;
use Math::Round;
use Date::Simple ('date', 'today');

use FantasyFlexHelpers;

my $week="season";
#espn
#my $ff_file_base = "season/ff_";
#my $ff_file_ext = "cbs.txt";

my $day_of_week = lc(DateTime->now()->day_name);
my $ff_file_base="";
my $ff_file_ext="ff_cbs.txt";

my $i=0;
my $max=35;
my $increment=40;
my $index=0;
my $data_link="http://fantasynews.cbssports.com/fantasyfootball/stats/weeklyprojections";
my $file = $ff_file_base . $ff_file_ext;
my $http;

my $curl = WWW::Curl::Easy->new;
my $response_body;
my $retcode;
my $response_code;

#print $file;
system "echo \"\" > $file";
#cbs
my @position=("QB","RB","WR","TE","K","DTS");
my $length = scalar(@position);
my $type="avg/standard";
my $rows="print_rows=9999";

$file = $ff_file_base . $ff_file_ext;
system "echo \"\" > $file";
#clean_file($file);
for($i = 0; $i <$length; $i++) {
  #Week
 $http = $data_link . "\/" . $position[$i] . "\/" . $week . "\/" . $type . "?&" . $rows;
 $curl->setopt(CURLOPT_HEADER,1);
 $curl->setopt(CURLOPT_URL, $http);
 my $response_body = '';
 $curl->setopt(CURLOPT_WRITEDATA,\$response_body);
 $retcode = $curl->perform;
 if ($retcode == 0) {
   print("Transfer went ok\n");
   $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
   # judge result and next action based on $response_code
   open(MYFILE, ">>$file");
   print MYFILE "$response_body";
   print MYFILE "-----\n";
   close(MYFILE);
  } else {
    # Error code, type of error, error message
    print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
  }
 #get_data($http,$file);
}
