#!/usr/bin/perl

use strict;
use warnings;

use HTML::TreeBuilder::XPath;
use Data::Dumper;
use DBI;
use DateTime;
use Math::Round;

use FantasyFlexHelpers;

my $host = "localhost";
my $database = "fantasyflex";
my $port = 5432;
my $user = "postgres";
my $pw = "jBds024jm\@CdxE";

my $week = 0;
my $source = 4;
my $source_name = 'fantasypros';
#my $day_of_week = lc(DateTime->now()->day_name);

#print "Today is " . $day_of_week. "\n";
my $day_of_week = "season";

# Establish a database connection
my $dbh = DBI->connect("dbi:Pg:database=$database;host=$host;port=$port;", $user, $pw)
  	or die ( "Couldn't connect to database: " . DBI->errstr );

print "Connected!";
# Change the path to the file you want to parse.
#my $tree = HTML::TreeBuilder::XPath->new_from_file('/Users/rhnickels/development/fantasy/parse/downloads/espn.html');
my $tree = HTML::TreeBuilder::XPath->new_from_file('../season/ff_fantasypros.txt');

# Lets validate that our labels are accurate and in the correct order before assuming statistical data

# Now that we have an array of labels, lets grab the player data
# Use a pattern rather than a string to match the possible player row classes
my $player_class_pattern = qr/^(?:table table-bordered table-condensed table-striped table-hover)$/;

my $player_count = 0;

foreach my $tables ($tree->look_down(_tag => 'table', 'id' =>'data') ){
    foreach my $player_row ($tables->look_down(_tag=>'tbody')->look_down(_tag => 'tr') ){
        # Assign the <a> containing the player's name to a variable
        my $team = 0;
        my $team_tag = ($player_row->look_down(_tag => 'td'))[0]->look_down(_tag => 'small');
        $team = $team_tag ? $team_tag->as_text() : 'UNK';
        # Assign the <a> containing the player's name to a variable
        my $ignore_class = qr/^.(?:fp-player-link)./;
        my $player_name_tag = ($player_row->look_down(_tag => 'td'))[0]->look_down(_tag => 'a', sub{ not $_[0]->look_down('class' => $ignore_class)});
        #print "$player_name_tag";
        #die;
        print "\n player is: " . $player_name_tag->as_text();
        my $projected_score = ($player_row->look_down(_tag => 'td'))[-1];
        #my $projected_score = $player_row->look_down(_tag => 'td', 'class' => qr/^(?:appliedPoints)$/);
        print "\n Projected score:";
        print $projected_score->as_text();
        print "\n";

        my $projected_score_rounded = nearest(1, $projected_score->as_text());
        # Add key->value pairs to the player_hash
        my %player_hash = (
            name => $player_name_tag->as_text(),
        );

        my $player_name = $player_hash{"name"};
        if($player_name eq 'E.J. Manuel') {
            $player_name = 'EJ Manuel';
        }

        if($player_name eq 'Taylor Yates') {
            $player_name = 'T.J. Yates';
        }

        if($player_name eq 'Christopher Ivory') {
            $player_name = 'Chris Ivory';
        }

        if($player_name eq 'Stevie Johnson') {
            $player_name = 'Steve Johnson';
        }

        # Dump out the player variables
        print "\n ";
        print Dumper %player_hash;
        my $player_id = 0;
        if($team eq '') {
            $player_id = find_player($player_name, $dbh);
        } else {
            $player_id = find_player_with_team($player_name, $team, $dbh);        
        }

        if ($player_id eq 0) {
            my $print_to_file = "Could not find " . $player_hash{"name"} . " who is projected for:  " . $projected_score_rounded;
            open (MYFILE, '>>' .$source_name . '_' . $day_of_week . '_noninserted.txt');
            print MYFILE "$print_to_file\n";
            close (MYFILE);
        } elsif($player_id eq 2) {
            my $print_to_file = "Found more than one instance of " . $player_hash{"name"} . " who is projected for:  " . $projected_score_rounded;
            open (MYFILE, '>>' .$source_name . '_' . $day_of_week . '_noninserted.txt');
            print MYFILE "$print_to_file\n";
            close (MYFILE);
        } else {   
            my $insert = save_or_update_season_projection($player_id, $week, $projected_score, $projected_score_rounded, $day_of_week, $source, $dbh);

            if($insert eq 0) {
                my $print_to_file = $player_hash{"name"} . " is projected for:  " . $projected_score_rounded . " but no gsis_id found";
                open (MYFILE, '>>' .$source_name . '_' . $day_of_week . '_noninserted.txt');
                print MYFILE "$print_to_file\n";
                close (MYFILE);
            }
        }

    }
}
print "DONE!";
