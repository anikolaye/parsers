#!/usr/bin/perl
use strict;
use warnings;
#use LWP::UserAgent;
#use HTTP::Request;
#use HTML::TreeBuilder;
use DateTime;
use WWW::Curl::Easy;
use Math::Round;
use Date::Simple ('date', 'today');

use FantasyFlexHelpers;

my $week="0";
#espn
#my $ff_file_base = "season/ff_";
#my $ff_file_ext = "cbs.txt";

my $day_of_week = lc(DateTime->now()->day_name);
my $ff_file_base="";
my $ff_file_ext="ff_scout.txt";

my $http;

my $curl = WWW::Curl::Easy->new;
my $response_body;
my $retcode;
my $response_code;

#cbs
my @position=(
  "QB",
  "RB",
  "WR",
  "TE",
  "K",
  "Def"
);
my $length = scalar(@position);

my $root="http://www.scout.com/api/fantasy/rankings?";
my $fromGet = "from=";
my $second = "&size=50&sortBy=FantasyPoints&viewType=players&";
my $positionsGet = "";
my $third = "&ppr=NON-PPR&qbTouchdowns=4&scoringMethodId=default&seasonYear=2015&";
my $weekGet = "week=" . $week;
my $last = "&scoringMethod=%7B%22receiveCatchesFactor%22%3A0%2C%22passingTouchdownsFactor%22%3A4%7D";
my $file = $ff_file_base . $ff_file_ext;

my $i=1;
my $j = 0;
my $max = 200;
my $index = 50;

system "echo \"\" > $file";
#clean_file($file);
  for($i = 0; $i <$length; $i++) {
    $j = 0;
    if($position[$i] eq "QB") {
      $max = 100;
    } elsif($position[$i] eq 'RB') {
      $max = 150;
    } elsif($position[$i] eq 'WR') {
      $max = 200;
    } elsif($position[$i] eq 'TE') {
      $max = 99;
    } elsif($position[$i] eq 'K') {
      $max = 50;
    } elsif($position[$i] eq 'Def') {
      $max = 32;
    }
    while($j < $max) {
      $positionsGet = "positions=%5B%22" . $position[$i] . "%22%5D";
      #Week
     $http =  $root . $fromGet . $j . $second . $positionsGet . $third . $weekGet . $last;
     $curl->setopt(CURLOPT_HEADER,1);
     $curl->setopt(CURLOPT_URL, $http);
     my $response_body = '';
     $curl->setopt(CURLOPT_WRITEDATA,\$response_body);
     $retcode = $curl->perform;
     if ($retcode == 0) {
       print("Transfer went ok\n");
       $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
       # judge result and next action based on $response_code
       open(MYFILE, ">>$file");
       print MYFILE "$response_body";
       print MYFILE "-----\n";
       close(MYFILE);
      } else {
        # Error code, type of error, error message
        print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
      }
      #get_data($http,$file);
      $j = $j + $index;
    }
  }
