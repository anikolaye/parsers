#!/usr/bin/perl
package FantasyFlexHelpers;
use strict;
use warnings;
use Exporter;
use Math::Round;
use Date::Simple ('date', 'today');
use DBI;


our @ISA= qw( Exporter );

# these CAN be exported.
our @EXPORT_OK = qw( get_current_week find_player find_player_with_team save_or_update_projection save_or_update_season_projection);

# these are exported by default.
our @EXPORT = qw( get_current_week find_player find_player_with_team save_or_update_projection save_or_update_season_projection);

sub get_current_week {
	my @nflWeeks = (
		"2015-09-14",
		"2015-09-21",
		"2015-09-28",
		"2015-10-05",
		"2015-10-12",
		"2015-10-19",
		"2015-10-26",
		"2015-11-02",
		"2015-11-09",
		"2015-11-16",
		"2015-11-23",
		"2015-11-30",
		"2015-12-07",
		"2015-12-14",
		"2015-12-21",
		"2015-12-28",
		"2016-01-04"
	);

	my $week = 0;
	for my $i (0 .. $#nflWeeks) {
		my $dateDiff = today() - date($nflWeeks[$i]);
		if( $dateDiff <= 0) {
			$week = $i + 1;
			last;
		}
	}
	return $week;
}

sub find_player {
	my $player = $_[0];
	my $dbh = $_[1];
	my $char = ' ';
	my $player_id = 0;
	$player = lc($player);

    my $sth = $dbh->prepare("SELECT count(*) FROM player WHERE LOWER(full_name) = ? AND position IN ('QB', 'RB', 'WR', 'TE', 'FB', 'K')");
    $sth->bind_param(1, $player);
    $sth->execute();
    my $count = $sth->fetchrow();
    if($count eq 2) {
    	return 2;
    }
    if($count eq 0) {
		$player = substr($player, 0, rindex($player, $char));
	    $sth = $dbh->prepare("SELECT count(*) FROM player WHERE LOWER(full_name) = ? AND position IN ('QB', 'RB', 'WR', 'TE', 'FB', 'K')");
	    $sth->bind_param(1, $player);
	    $sth->execute();
	    $count = $sth->fetchrow();
    } 
    if($count eq 1) {
        $sth = $dbh->prepare("SELECT player_id FROM player WHERE LOWER(full_name) = ? AND position IN ('QB', 'RB', 'WR', 'FB', 'TE', 'K')");
        $sth->bind_param(1, $player);
        $sth->execute();
        $player_id = $sth->fetchrow();
    }

    return $player_id;
}

sub find_player_with_team {
	my $player = $_[0];
	my $team = $_[1];
	my $dbh = $_[2];
	my $char = ' ';
	my $player_id = 0;
	$player = lc($player);
	$team = lc($team);

    my $sth = $dbh->prepare("SELECT count(*) FROM player WHERE LOWER(full_name) = ? AND lower(team) = ? AND position IN ('QB', 'RB', 'WR', 'TE', 'FB', 'K')");
    $sth->bind_param(1, $player);
    $sth->bind_param(2, $team);
    $sth->execute();
    my $count = $sth->fetchrow();
    if($count eq 2) {
    	return 2;
    }
    if($count eq 0) {
		$player = substr($player, 0, rindex($player, $char));
	    $sth = $dbh->prepare("SELECT count(*) FROM player WHERE LOWER(full_name) = ? AND lower(team) = ? AND position IN ('QB', 'RB', 'WR', 'TE', 'FB', 'K')");
	    $sth->bind_param(1, $player);
        $sth->bind_param(2, $team);
	    $sth->execute();
	    $count = $sth->fetchrow();
    } 
    if($count eq 1) {
        $sth = $dbh->prepare("SELECT player_id FROM player WHERE LOWER(full_name) = ? AND lower(team) = ? AND position IN ('QB', 'RB', 'WR', 'FB', 'TE', 'K')");
        $sth->bind_param(1, $player);
        $sth->bind_param(2, $team);
        $sth->execute();
        $player_id = $sth->fetchrow();
    }
    
    return $player_id;
}

sub save_or_update_projection {
	my $player_id = $_[0];
	my $week = $_[1];
	my $projected_score = $_[2];
	my $projected_score_rounded = $_[3];
	my $day_of_week = $_[4];
	my $source = $_[5];
	my $dbh = $_[6];

	my $season_year = 2015;
	my $sth = $dbh->prepare("select gsis_id from player p, game g WHERE p.player_id = ? AND (g.home_team = p.team OR g.away_team = p.team) AND g.week = ? AND g.season_type = 'Regular' AND g.season_year = 2015 AND g.finished = 'f'");
	$sth->bind_param(1, $player_id);
	$sth->bind_param(2, $week);
	$sth->execute();

	my $gsis_id = $sth->fetchrow();
	if(defined $gsis_id) {
		$sth = $dbh->prepare("SELECT count(*) FROM player_projections WHERE source_id = ? AND player_id = ? AND gsis_id = ?");

		$sth->bind_param(1, $source);
		$sth->bind_param(2, $player_id);
		$sth->bind_param(3, $gsis_id);

		$sth->execute();
		my ($projection_count) = $sth->fetchrow_array();
		if($projection_count == 0) {
		    print "Did not find projection for player_id " . $player_id . "\n";
		    $sth = $dbh->prepare("INSERT INTO player_projections (source_id, player_id, season_year, gsis_id, " . $day_of_week . ") VALUES(?, ?, ?, ?, ?)");
		    $sth->bind_param(1, $source);
		    $sth->bind_param(2, $player_id);
		    $sth->bind_param(3, $season_year);
		    $sth->bind_param(4, $gsis_id);
		    if($projected_score->as_text() eq "--") {
		        $sth->bind_param(5, 0);
		    } else {
		        $sth->bind_param(5, $projected_score_rounded);
		    }


		    $sth->execute();
		} else {
		    print "Found projection for player_id " . $player_id . "\n";
		    print "Updating for today";
		    $sth = $dbh->prepare("SELECT id FROM player_projections WHERE source_id = ? AND player_id = ? AND gsis_id = ?");


		    $sth->bind_param(1, $source);
		    $sth->bind_param(2, $player_id);
		    $sth->bind_param(3, $gsis_id);

		    $sth->execute();
		    my $id = $sth->fetchrow();
		    $sth = $dbh->prepare("UPDATE player_projections
		                                SET " . $day_of_week . "= ?
		                                WHERE id = ?");
		        if($projected_score->as_text() eq "--") {
		            $sth->bind_param(1, 0);
		        } else {
		            $sth->bind_param(1, $projected_score_rounded);
		        }
		    $sth->bind_param(2, $id);

		    $sth->execute();
		}
		return 1;
	} else {
		return 0;
	}
}

sub save_or_update_season_projection {
	my $player_id = $_[0];
	my $week = $_[1];
	my $projected_score = $_[2];
	my $projected_score_rounded = $_[3];
	my $day_of_week = $_[4];
	my $source = $_[5];
	my $dbh = $_[6];

	my $season_year = 2015;

	my $sth = $dbh->prepare("SELECT count(*) FROM player_projections WHERE gsis_id is null AND source_id = ? AND player_id = ? AND season_year = ?");

	$sth->bind_param(1, $source);
	$sth->bind_param(2, $player_id);
	$sth->bind_param(3, $season_year);

	$sth->execute();
	my ($projection_count) = $sth->fetchrow_array();
	if($projection_count == 0) {
	    print "Did not find projection for player_id " . $player_id . "\n";
	    $sth = $dbh->prepare("INSERT INTO player_projections (source_id, player_id, season_year, " . $day_of_week . ") VALUES(?, ?, ?, ?)");
	    $sth->bind_param(1, $source);
	    $sth->bind_param(2, $player_id);
	    $sth->bind_param(3, $season_year);
	    if($projected_score->as_text() eq "--") {
	        $sth->bind_param(4, 0);
	    } else {
	        $sth->bind_param(4, $projected_score_rounded);
	    }


	    $sth->execute();
	} else {
	    print "Found projection for player_id " . $player_id . "\n";
	    print "Updating for today";
	    $sth = $dbh->prepare("SELECT id FROM player_projections WHERE gsis_id is null AND source_id = ? AND player_id = ? AND season_year = ?");


	    $sth->bind_param(1, $source);
	    $sth->bind_param(2, $player_id);
	    $sth->bind_param(3, $season_year);

	    $sth->execute();
	    my $id = $sth->fetchrow();
	    $sth = $dbh->prepare("UPDATE player_projections
	                                SET " . $day_of_week . "= ?
	                                WHERE id = ?");
	        if($projected_score->as_text() eq "--") {
	            $sth->bind_param(1, 0);
	        } else {
	            $sth->bind_param(1, $projected_score_rounded);
	        }
	    $sth->bind_param(2, $id);

	    $sth->execute();
	}
	return 1;
}
