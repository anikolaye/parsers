#!/usr/bin/perl
use strict;
use warnings;
#use LWP::UserAgent;
#use HTTP::Request;
#use HTML::TreeBuilder;
use DateTime;
use WWW::Curl::Easy;
use Math::Round;
use Date::Simple ('date', 'today');

use FantasyFlexHelpers;

my $week=get_current_week();
#espn
my $day_of_week = lc(DateTime->now()->day_name);
my $ff_file_base="";
#my $ff_file_base = "season/ff_";
my $ff_file_ext="ff_fox.txt";

my $i=1;
my $data_link="http://www.foxsports.com/fantasy/football/commissioner/Research/Projections.aspx?position=-1&split=3&playerSearchStatus=1&page=";
my $file = $ff_file_base . $ff_file_ext;
my $http;

my $curl = WWW::Curl::Easy->new;
my $response_body;
my $retcode;
my $response_code;
#nfl.com
#positions in same order as cbs positions
my $max = 43;
#split 3 = season
#split 4 = week?
#http://www.foxsports.com/fantasy/football/commissioner/Research/Projections.aspx?position=-1&split=3&playerSearchStatus=1&page=
#SEASON
#clean_file($file);
system "echo \"\" > $file";
while($i < $max) {
 $http = $data_link . $i;

 $curl->setopt(CURLOPT_HEADER,1);
 $curl->setopt(CURLOPT_URL, $http);
 my $response_body = '';
 $curl->setopt(CURLOPT_WRITEDATA,\$response_body);
 $retcode = $curl->perform;
 if ($retcode == 0) {
   print("Transfer went ok\n");
   $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
   # judge result and next action based on $response_code
   open(MYFILE, ">>$file");
   print MYFILE "$response_body";
   print MYFILE "-----\n";
   close(MYFILE);
  } else {
    # Error code, type of error, error message
    print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
  }
  $i = $i + 1;
  #get_data($http, $file, $curl)
}
