#!/usr/bin/perl
use strict;
use warnings;
#use LWP::UserAgent;
#use HTTP::Request;
#use HTML::TreeBuilder;
use DateTime;
use WWW::Curl::Easy;
use Math::Round;
use Date::Simple ('date', 'today');

use FantasyFlexHelpers;

my $week=get_current_week();
#espn
my $day_of_week = lc(DateTime->now()->day_name);
#my $ff_file_base="weeks/" . $week . "/" . $day_of_week . "_";
my $ff_file_base = "";
my $ff_file_ext="ff_fantasysharks.txt";

my $data_link="http://www.fantasysharks.com/apps/Projections/SeasonProjections.php?pos=ALL&format=json&l=1";
my $file = $ff_file_base . $ff_file_ext;
my $http;

my $curl = WWW::Curl::Easy->new;
my $response_body;
my $retcode;
my $response_code;
system "echo \"\" > $file";
$http = $data_link;

$curl->setopt(CURLOPT_HEADER,1);
$curl->setopt(CURLOPT_URL, $http);
$curl->setopt(CURLOPT_WRITEDATA,\$response_body);
$retcode = $curl->perform;
if ($retcode == 0) {
 print("Transfer went ok\n");
 $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
 # judge result and next action based on $response_code
 open(MYFILE, ">>$file");
 print MYFILE "$response_body";
 print MYFILE "-----\n";
 close(MYFILE);
} else {
  # Error code, type of error, error message
  print("An error happened: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
}
